import i18n from "i18next";
import { reactI18nextModule } from "react-i18next";

import translationEN from './locales/EN/translation.json';
import translationFR from './locales/FR/translation.json';
import translationMG from './locales/MLG/translation.json';

// the translations
const resources = {
  en: {
    translation: translationEN
  },
  fr: {
    translation: translationFR
  },
  mg: {
    translation: translationMG
  }
};

i18n
  .use(reactI18nextModule) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "mg",
    // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
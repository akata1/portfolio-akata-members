import React from "react";

import project1 from "../img/serv/css.png";
import project2 from "../img/serv/aangular.png";
import project3 from "../img/serv/NodeJS.jpg";
import project4 from "../img/serv/react.jfif";
import project5 from "../img/serv/sass.jpg";
import project6 from "../img/serv/symfony.png";
import { withNamespaces } from 'react-i18next';
const projects = [
  {
    name: "Angular",
    image: project2,
  },
  {
    name: "React",
    image: project4,
  },
  {
    name: "SASS",
    image: project5,
  },
  {
    name: "CSS",
    image: project1,
  }
];
const projects1 = [
  {
    name: "NodeJS",
    image: project3,
  },
  {
    name: "Symfony",
    image: project6,
  }
];
export function Services({ t }) {
    return (
      <section id="services" className="about-mf sect-pt4 route">
          <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="box-shadow-full">
        <div className="title-box text-center">
            <h3 className="title-a">{t('service')}</h3>
            <div className="line-mf"></div>
                <div className="row">
                <div className="col row">
                              {/* loop img */}
              <div className="col-md-12 mt-4">
              <h2 className="">Front-End</h2>
              </div>
              {projects.map((project, i) => (
            <div className="col-md-6 right1 mt-4">
              <div className="col-md-8">
              <div className="work-box">
              <a href={project.image} data-lightbox="gallery-vmarine">
                  <div className="work-img">
                    <img src={project.image} alt="" className="img-fluid" />
                  </div>
                </a>
               </div>
               </div>     
                  </div>
              ))}
                  {/* fin loop */}
                  </div>
                <div className="col row"> {/* loop img */}
                <div className="col-md-12 mt-4">
              <h2 className="">Back-End</h2>
              </div>
              {projects1.map((projectk, i) => (
            <div className="col-md-12 right mt-4">
              <div className="col-md-5">
              <div className="work-box">
              <a href={projectk.image} data-lightbox="gallery-vmarine">
                  <div className="work-img">
                    <img src={projectk.image} alt="" className="img-fluid" />
                  </div>
                </a>
               </div>
               </div>     
                  </div>
              ))}
                  {/* fin loop */}
                  </div>
          </div>
        </div>
        </div>
        </div>
        </div>
        </div>
      </section>
    );
}
export default withNamespaces()(Services);

import React from "react";
import stock from "../img/serv/bodge.jpg";
import { withNamespaces } from 'react-i18next';

  export function Other({ t }){
    return (
      <section id="other" className="about-mf sect-pt4 route">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="box-shadow-full">
                <div className="title-box text-center">
                  <h6 className="title-a">{t('Availability.title')}</h6>
                  <div className="line-mf"></div>
                    <div className="row text-center">
                     <div className="col-md-6">
                      <h4 className="my-3">{t('Availability.Subtitle')}</h4>
                        <p className="text-muted">{t('Availability.work')}</p>
                        <p className="text-muted">{t('Availability.letter')}</p>                    
                        </div>
                            <div className="col-md-6">
                             <h4 className="my-3">{t('Prix.title')}</h4>
                              <p className="text-muted">{t('Prix.price')} {t('Prix.time')}</p>
                              <p className="text-muted">{t('Prix.type')}</p>
                            </div>
                      </div>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
}

export default withNamespaces()(Other);

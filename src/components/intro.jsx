import React from "react";
import "./stars.scss";
import Typed from "react-typed";
import { withNamespaces } from 'react-i18next';
export function Intro({ t }){
    return (
      // <div id="home" className="intro route bg-image " style={{backgroundImage: "url("+bigImage+")"}}>
      <div id="home" className="intro route bg-image background">
        <div id="stars" />
        <div id="stars2" />
        <div id="stars3" />

        <div className="intro-content display-table">
          <div className="table-cell">
            <div className="container">
              <h4 className="intro-title mb-4">{t('hello')}</h4>
              <p className="intro-subtitle">
                <span className="text-slider-items"></span>
                <strong className="text-slider">
                  <Typed
                    strings={[
                      "Full stack Freelance Developer"
                    ]}
                    typeSpeed={80}
                    backDelay={1100}
                    backSpeed={30}
                    loop
                  />
                </strong>
              </p>
              <p className="pt-3">
                <a
                  className="btn btn-primary btn js-scroll px-4"
                  href="#work"
                  role="button"
                >
                  {t('button')}
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
}

export default withNamespaces()(Intro);

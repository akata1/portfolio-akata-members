import React from "react";
import imageOverlay from "../img/earth.jpg";
import { MDBContainer} from "mdbreact";
import './stars.scss';
import i18n from '../i18n';
import { withNamespaces } from 'react-i18next';
function Contact({ t }){
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  }

    return (
      <section
        className="paralax-mf footer-paralax bg-image sect-mt4 route"
        style={{ backgroundImage: "url(" + imageOverlay + ")" }}
      >
        <div className="overlay-mf"></div>
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="contact-mf">
                <div id="contact" className="box-shadow-full">
                <div className="title-box text-center">
                <h6 className="title-a">{t('Contacts')}</h6>
                <div className="line-mf"></div>
              </div>
                  <div className="row">
                    <div className="col-md-6 centerTxt">
                    <div className="socials">
                      <p className="text-muted"><i className="fa fa-phone" aria-hidden="true"></i>  +261349375988</p>
                      <p className="text-muted"><i className="fa fa-envelope" aria-hidden="true"></i>  ranto10tavela@gmail.com</p>
                      <p className="text-muted"><i className="fa fa-skype" aria-hidden="true"></i>  Man Assez</p>
                    </div>
                    </div>
                    <div className="col-md-6">
                      <div className="socials">
                        <ul className="d-flex flex-column align-items-end justify-content-between">
                          <li className="padd">
                            <a href="#" target="_blank"rel="noopener noreferrer">
                              <span className="ico-circle">
                                <i className="ion-social-linkedin" size="small"></i>
                              </span>
                            </a>
                          </li>
                          <li className="padd">
                            <a
                              href=""
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="ico-circle">
                                <i className="ion-social-github kely"></i>
                              </span>
                            </a>
                          </li>
                          <li className="padd">
                            <a
                              href=""
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="ico-circle">
                                <i className="ion-social-facebook"></i>
                              </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="footer-copyright text-center py-3">
                      <MDBContainer fluid>
                        &copy; {new Date().getFullYear()} Copyright: <a href="https://www.mdbootstrap.com"> Bodge </a>
                      </MDBContainer>
                      <MDBContainer fluid>
                        <a className="mrl" onClick={() => changeLanguage('mg')} href="#">MLG</a><a className="mrl" onClick={() => changeLanguage('fr')} href="#">FR</a><a className="mrl" onClick={() => changeLanguage('en')} href="#">EN</a>
                      </MDBContainer>
                </div>
                  {/* eto  */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }

export default withNamespaces()(Contact);

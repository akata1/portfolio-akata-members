import React from "react";
import stock from "../img/serv/bodge.jpg";
import { withNamespaces } from 'react-i18next';
export function About({ t }){
  // const { i18n } = useTranslation();
  // function changeLang(params) {
  //   i18n.changeLanguage(params);
  // }
    return (
      <section id="about" className="about-mf sect-pt4 route">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="box-shadow-full">
                <div className="row">
                  <div className="col-md-6">
                    <div className="row">
                      <div
                        className="col-sm-6 col-md-5"
                        style={{ margin: "0 auto" }}
                      >
                        <div
                          className="about-img"
                          style={{ textAlign: "center" }}
                        >
                          <img
                            className="img-fluid rounded b-shadow-a"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
              <div className="col-md-10">
              <div className="work-box">
              <a href={stock} data-lightbox="gallery-vmarine">
                  <div className="work-img">
                    <img src={stock} alt="" className="img-fluid" />
                  </div>
                </a>
               </div>
               </div>     
                  </div>
                  <div className="col-md-6">
                    <div className="about-me pt-4 pt-md-0">
                      <div className="title-box-2">
                        <h6 className="title-left">{t('About.title')}</h6>
                      </div>
                          <p className="lead">
                          {t('About.content1')}                         
                           </p>
                        <p className="lead">
                        {t('About.content2')}                        
                         </p>
                    </div>
                  </div>
                  <div className="row text-center">
                    <div className="col-md-4">
                        <h4 className="my-3"> {t('one.subtitle')}</h4>
                        <p className="text-muted">{t('one.content1')}</p>
                        <p className="text-muted">{t('one.content2')}</p>                    
                    </div>
                    <div className="col-md-4">
                        <h4 className="my-3">{t('two.subtitle')}</h4>
                        <p className="text-muted">{t('two.content1')}</p>
                        <p className="text-muted">{t('two.content2')}</p>
                    </div>
                    <div className="col-md-4">
                        <h4 className="my-3">{t('three.subtitle')}</h4>
                        <p className="text-muted">{t('three.content1')}</p>
                        <p className="text-muted">{t('three.content2')}</p>                    
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }

export default withNamespaces()(About);
